{(C)2021 PowerSchool Group LLC.
  All Rights Reserved.  This program is PROPRIETARY and CONFIDENTIAL
  information of PowerSchool Group LLC, and may not be disclosed or used except
  as expressly authorized in a license agreement controlling such use and
  disclosure.  Unauthorized use of this program will result in legal
  proceedings, civil damages and possible criminal prosecution.}

{|----------------------------------------------------------------------------|
 | System name -- PAYROLL REGISTER                                            |
 |----------------------------------------------------------------------------|
 | Procedure ---- Payroll register Functions                                  |
 | Source module- payrollregister.4gl                                         |
 |----------------------------------------------------------------------------|
 | Program link -                                                             |
 |----------------------------------------------------------------------------|
 | ESD REV #   DATE        BY       COMMENTS                                  |
 | EFIN-53537  06/08/2021  suresh   Initial Development Payroll Register.     |
 | EFIN-69331  10/06/2021  suresh   Payroll Register to have the Batch Type   |
 |                                  parameter in the Search Criteria          |
 |----------------------------------------------------------------------------|}

IMPORT os
IMPORT util
SCHEMA finplus

GLOBALS

   DEFINE r_hrm_prof  RECORD LIKE hrm_prof.*,
          r_site_info RECORD LIKE site_info.*,
          where_part     STRING,
          print_emplyee  VARCHAR(20),
          query_text     STRING,
          i              SMALLINT,
          p_rpt_name     CHAR(80),
          p_rpt_dest     CHAR(1),
          p_prt_cmd      CHAR(60),
          run_str        CHAR(132),
          progress_total INTEGER,
          glo_error_log  CHAR(80)
   DEFINE adv_success    SMALLINT,
          adv_message    STRING,
          adv_user_crit  STRING,
          adv_from       STRING,
          from_part      STRING,
          adv_where      STRING

   DEFINE sec RECORD
          uid       LIKE sectb_user.uid,
          lname     LIKE sectb_user.lname,
          fname     LIKE sectb_user.fname,
          building  LIKE sectb_user.building,
          bld_group LIKE sectb_user.bld_group,
          dept      LIKE sectb_user.dept,
          orgn      LIKE sectb_user.orgn,
          fund      LIKE sectb_user.fund,
          project   LIKE sectb_user.project,
          qwarn     LIKE sectb_user.qwarn,
          qlimit    LIKE sectb_user.qlimit,
          package   LIKE sectb_resource.package,
          subpack   LIKE sectb_resource.subpack,
          func      LIKE sectb_resource.func
      END RECORD,
      first_read SMALLINT,
      dbengine   CHAR(3),
      username   CHAR(40)

   DEFINE col1, col2 SMALLINT
   DEFINE col3       SMALLINT                                   #EFIN-69331
   DEFINE gBldgWhere CHAR(200)
   DEFINE gDeptWhere LIKE sectb_user.dept
   DEFINE gMATCHES   STRING
   DEFINE percent_char, 
          asterisk_char CHAR(1)
   DEFINE user_inter    SMALLINT
   DEFINE r_fam_prof  RECORD LIKE fam_prof.*
   DEFINE lr_checkrec RECORD LIKE checkrec.*
   DEFINE r_transact  RECORD LIKE transact.*
   DEFINE reenter        SMALLINT
   DEFINE progress_text  STRING
   DEFINE progress_text1 STRING                                 #FIN-69331
   DEFINE info1          CHAR(80)
   DEFINE prt_str_buff   STRING
   DEFINE hnd1 om.SaxDocumentHandler
   DEFINE exist  SMALLINT
   DEFINE h_date DATE
   DEFINE output_preview CHAR(1)
   DEFINE tmpFileName    STRING
   TYPE t_fileinfo RECORD
        filename STRING,
        filesize INTEGER,
        modtime  STRING
   END RECORD
   DEFINE files DYNAMIC ARRAY OF t_fileinfo
   DEFINE idx         INTEGER
   DEFINE output_type CHAR(1)
   DEFINE reportFileName, 
          tmpFilename STRING
   DEFINE mDataOption CHAR(1)
   DEFINE h_yr        CHAR(2)
   CONSTANT cOutputScreen = "t"
   CONSTANT cOutputFile = "d"
   CONSTANT cPDFType = "p"
   CONSTANT cXLSXType = "x"
   CONSTANT cCurrentDir = "."
   CONSTANT cReportDir = "..%1reports%1."

   DEFINE r_paygroups   RECORD LIKE paygroups.*
   DEFINE arr_paygroups DYNAMIC ARRAY OF RECORD
          pay_run  LIKE paygroups.pay_run,
          run_desc LIKE paygroups.run_desc
   END RECORD
   DEFINE r_faorgn  RECORD LIKE faorgn.*
   DEFINE r_account RECORD LIKE faaccount.*
   DEFINE pa_gl DYNAMIC ARRAY OF RECORD
          trans_date  LIKE transact.trans_date,
          t_c         LIKE transact.t_c,
          je_number   LIKE transact.invoice,
          description LIKE transact.description,
          debit       LIKE transact.trans_amt,
          credit      LIKE transact.trans_amt
   END RECORD
   DEFINE row_cnt, row_id INTEGER
   DEFINE tot1, tot2, tot3, tot4,
          tot5, bal       DECIMAL(12, 2)
   DEFINE r_hdr RECORD
          line1, line2, line3, 
          line4   CHAR(132),
          PAGE    CHAR(20),
          pagecol INTEGER
   END RECORD,
      page_width INTEGER,
      titlex, modulename, 
      info1           CHAR(80)
   DEFINE cal         CHAR(2)
   DEFINE chrr        CHAR(20)
   DEFINE payrun_part STRING
   DEFINE l_debit,
          l_credit,
          tot_amt,
          total_debit,
          grp_debit,
          grp_credit,
          total_credit,
          grd_total_debit,
          grd_total_credit DECIMAL(12, 2)

   DEFINE pay_date_from,pay_date_to DATE
   DEFINE r_employee RECORD LIKE employee.*

   DEFINE r_payroll RECORD
          empl_no    LIKE detdist.empl_no,
          f_name     LIKE employee.f_name,
          m_name     LIKE employee.m_name,
          l_name     LIKE employee.l_name,
          rec_type   STRING,
          pay_run    LIKE detdist.pay_run,
          pay_date   LIKE detdist.pay_date,
          acct       LIKE detdist.acct,
          Pay_Fund_Account STRING,
          Debit            DECIMAL(12,2),
          Credit           DECIMAL(12,2)
   END RECORD
   DEFINE r_faaccount  RECORD LIKE faaccount.*
   DEFINE r_detdist    RECORD LIKE detdist.*
   DEFINE r_transact   RECORD LIKE transact.*
   DEFINE currdate      DATE
   DEFINE r_fam_prof    RECORD LIKE fam_prof.*
   DEFINE currtime      DATETIME HOUR TO SECOND
   DEFINE fund_title    STRING
   DEFINE fullname      CHAR(30)
   DEFINE str_pay_date  STRING
   DEFINE pay_run_desc  STRING
   DEFINE empl_name     STRING
   DEFINE empl_no       STRING
   DEFINE empl_no_xls   STRING
   DEFINE mDataOption   CHAR(1)
   DEFINE pay_run_desc  STRING
   DEFINE empl_name     STRING
   DEFINE empl_name_xls STRING
   DEFINE empl_num      STRING
   DEFINE exit_button   SMALLINT
   DEFINE selected_pay_run  LIKE paygroups.pay_run
   DEFINE batch_type        STRING                              #EFIN-69331
   CONSTANT mModuleName = "libLoadCombos", mBubbleText = ":\n   "

END GLOBALS

DEFINE UsingMultiLocTax SMALLINT

MAIN

   DEFINE lintProcStatus  INTEGER
   DEFINE lstrProcMessage STRING
   DEFINE path_string     STRING
   CALL dbiStartDatabase("") RETURNING lintProcStatus, dbengine
   IF NOT lintProcStatus THEN
      CALL libErrDialog("Error", "Cannot connect to database", "stop", "OK")
      EXIT PROGRAM
   END IF
   CALL libGetLogin() RETURNING lintProcStatus, lstrProcMessage, username
   IF NOT lintProcStatus THEN
      CALL libErrDialog("Error", lstrProcMessage, "stop", "OK")
      EXIT PROGRAM
   END IF
   ## Changed chkLockEntry
   IF NOT libFPSCheckProgramLock("HRM", "Payroll Check Maintenance") THEN
      EXIT PROGRAM
   END IF
   CALL dbiSetLockWait(dbengine)

   LET first_read = TRUE
   DEFER INTERRUPT
   CALL set_opt()
   CALL libExpandPath(
      "$FASLOGDIR/payrollregister.log")
      RETURNING lintProcStatus, lstrProcMessage, glo_error_log
   IF NOT lintProcStatus THEN
      CALL libErrDialog(
         "Program Error", lstrProcMessage, "stop", "Exit Program")
      ## Changed EXIT PROGRAM
      CALL libExitProgram()
   END IF

   CALL libExpandPath(
      "$libt/libActionDefault.4ad")
      RETURNING lintProcStatus, lstrProcMessage, path_string
   IF NOT lintProcStatus THEN
      CALL libErrDialog(
         "Program Error", lstrProcMessage, "stop", "Exit Program")
      ## Changed EXIT PROGRAM
      CALL libExitProgram()
   END IF
   CALL ui.Form.setDefaultInitializer("libforminitializer")
   CALL ui.Interface.loadActionDefaults(path_string)
   CALL libExpandPath(
      "$libt/libStyleSheet.4st")
      RETURNING lintProcStatus, lstrProcMessage, path_string
   IF NOT lintProcStatus THEN
      CALL libErrDialog(
         "Program Error", lstrProcMessage, "stop", "Exit Program")
      ## Changed EXIT PROGRAM
      CALL libExitProgram()
   END IF
   CALL ui.Interface.loadStyles(path_string)

   CALL startlog(glo_error_log)
   WHENEVER ERROR CALL error_trap

   ## Added ON CLOSE APPLICATION
   OPTIONS ON CLOSE APPLICATION CALL libExitProgram

   SELECT * INTO r_hrm_prof.* FROM hrm_prof
   SELECT * INTO r_site_info.* FROM site_info #4.2.21 =>
   IF (SQLCA.SQLCODE = NOTFOUND) OR (r_site_info.site_code) IS NULL THEN
      LET r_site_info.site_code = "   "
   END IF
   LET percent_char = "%"
   LET asterisk_char = "*"

   LET UsingMultiLocTax = fincustomEntryExists("LocalTaxMulti")
   LET exit_button = FALSE
   CALL init_all("HRM")
   IF sec_init() THEN

      CALL parse_crit(
         "regtb_building.building", sec.bld_group)
         RETURNING gBldgWhere
      CASE dbengine
         WHEN "IFX"
            LET gMATCHES = "MATCHES"
            LET gDeptWhere = sec.dept
         WHEN "MSV"
            LET gDeptWhere =
               libReplaceString(sec.dept, asterisk_char, percent_char)
            LET gMATCHES = "LIKE"
      END CASE
      WHILE NOT(exit_button)
	     CALL payrollregister()
	  END WHILE
     
   ELSE
      ERROR "No security authorization"
   END IF

   #deleted extraneous call to payrollregister
   ##Changed chkLockExit
   CALL libExitProgram() #

END MAIN

PUBLIC FUNCTION payrollregister()

   DEFINE lProcStatus       SMALLINT
   DEFINE lMessage          STRING
   DEFINE mWindowOpened     SMALLINT
   DEFINE progress_text     STRING
   DEFINE lintProcStatus    INTEGER
   DEFINE lstrProcMessage   STRING
   DEFINE reenter           SMALLINT
   DEFINE lback_flag        BOOLEAN
   DEFINE lrpt_description1 STRING
   DEFINE lhdr_where_part   CHAR(500)
   DEFINE lrun_desc         STRING
   DEFINE ltitle            STRING
   DEFINE lemployee_text    STRING
   DEFINE lpayrunparam      STRING
   DEFINE lfamlowequ        STRING
   DEFINE lfamhighequ       STRING
   DEFINE lfamlowrev        STRING
   DEFINE lfamhighrev       STRING
   DEFINE lvoid_man         STRING                              #EFIN-69331
   DEFINE pComboBox ui.ComboBox

   LET int_flag = FALSE
   OPEN WINDOW payrollregister WITH FORM "payrollregister"

      CALL libLoadComboParam("paygroups.pay_run","paygroups","pay_run","run_desc",payrun_part)
                    RETURNING lProcStatus, lMessage

   CALL libSetText("Payroll Register Report")
   CALL libMakeTopMenu("STANDARD.favorites")
      RETURNING lintProcStatus, lstrProcMessage
   LET reenter = TRUE

   WHILE reenter
      CLEAR FORM
      #This information gets generated report screen
      LET lrpt_description1 = "This report will produce payroll register."

      DISPLAY lrpt_description1 TO lrpt_description1
      DISPLAY "information" TO formonly.image1

      INITIALIZE r_paygroups.pay_run TO NULL
      INITIALIZE r_employee.empl_no TO NULL

      IF num_args() == 2 THEN
         LET lpayrunparam = arg_val(1)
         LET r_paygroups.pay_run = arg_val(2)
      END IF

      LET r_detdist.void_man = "P"                              #EFIN-69331

      INPUT BY NAME r_detdist.void_man,                         #EFIN-69331
         r_paygroups.pay_run ,r_employee.empl_no
         WITHOUT DEFAULTS

         BEFORE INPUT
            IF r_paygroups.pay_run IS NOT NULL THEN
               CALL DIALOG.setFieldActive( "paygroups.pay_run", false)
            END IF

            AFTER INPUT
                IF SQLCA.SQLCODE != NOTFOUND THEN
                    DISPLAY BY NAME r_paygroups.pay_run
                END IF

                IF LENGTH(r_paygroups.pay_run) = 0 THEN
                    ERROR "Select a pay run"
                    NEXT FIELD pay_run
                END IF

                IF SQLCA.SQLCODE = NOTFOUND THEN
                    LET r_paygroups.pay_run = NULL
                    DISPLAY r_paygroups.pay_run TO scr_payroll.*
                END IF
            AFTER FIELD void_man                                #EFIN-69331 -->
               IF r_detdist.void_man IS NULL THEN
                  ERROR "Select Type of Batch.."
                  NEXT FIELD void_man
               END IF                                           #EFIN-69331 <--

            AFTER FIELD pay_run
               IF r_paygroups.pay_run IS NULL THEN
                  ERROR "Select pay run.."
                  NEXT FIELD pay_run
               END IF

         ON ACTION CANCEL
            LET int_flag = TRUE
            EXIT INPUT
         ON ACTION EXIT
            LET int_flag = TRUE
            EXIT INPUT
         ON ACTION fav_launcher
            CALL libShowFavorites() RETURNING lProcStatus, lMessage
         ON ACTION doc_launcher
            CALL libShowDocuments() RETURNING lProcStatus, lMessage
         ON ACTION wf_launcher
            CALL libShowActivities() RETURNING lProcStatus, lMessage
         ON ACTION add_favorite
            CALL libAddFavorite("HRM") RETURNING lProcStatus, lMessage
         ON ACTION delete_favorite
            CALL libDeleteFavorite("HRM") RETURNING lProcStatus, lMessage
         ON ACTION spiClose
            IF libConfirmQuit() THEN
               CALL libCloseProgram()
            END IF

            BEFORE field empl_no
            CALL LoadEmployeeNoCombo(NULL)

      END INPUT

      IF int_flag = TRUE THEN                                   
         LET int_flag = FALSE
         LET exit_button = TRUE
         RETURN
      END IF

      LET where_part = ""
         CONSTRUCT where_part ON detdist.pay_date
                FROM s_paydate.*

         ON ACTION CANCEL
            LET int_flag = TRUE
              EXIT CONSTRUCT
         ON ACTION exit
            LET int_flag = TRUE
            EXIT CONSTRUCT
         ON ACTION fav_launcher
            CALL libShowFavorites() RETURNING lProcStatus, lMessage
         ON ACTION doc_launcher
            CALL libShowDocuments() RETURNING lProcStatus, lMessage
         ON ACTION wf_launcher
            CALL libShowActivities() RETURNING lProcStatus, lMessage
            ##script change end
         ON ACTION add_favorite
            CALL libAddFavorite("HRM") RETURNING lProcStatus, lMessage

         ON ACTION delete_favorite
            CALL libDeleteFavorite("HRM") RETURNING lProcStatus, lMessage

         ON ACTION spiClose
            IF libConfirmQuit() THEN
               CALL libCloseProgram()
            END IF

         BEFORE FIELD pay_date
            CALL LoadPaydateCombo(NULL)

        END CONSTRUCT


      IF int_flag THEN
         LET int_flag = FALSE 
         IF lback_flag = TRUE THEN
            LET exit_button = FALSE 
         ELSE
            LET reenter = FALSE
         END IF
         CONTINUE WHILE
      END IF

            LET lemployee_text = ""
            IF r_employee.empl_no IS NOT NULL THEN 
                IF r_employee.empl_no = 0 THEN
            
                ELSE
                   LET lemployee_text = " and employee.empl_no  = ",r_employee.empl_no
                END IF
            END IF
         INITIALIZE lfamlowequ, lfamhighequ, lfamlowrev, lfamhighrev TO NULL

         SELECT low_equ, hi_equ, low_rev, hi_rev
            INTO lfamlowequ, lfamhighequ, lfamlowrev, lfamhighrev
         FROM fam_prof

         LET  lvoid_man = r_detdist.void_man                    #EFIN-69331 -->
         
            IF lvoid_man = 'P' THEN
               LET batch_type = " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') "
            END IF
            IF lvoid_man = 'R' THEN
               LET batch_type = " and detdist.redist in ('R', 'F', 'P') "
            END IF
            IF lvoid_man = 'M' THEN 
               LET batch_type = " and detdist.void_man = 'M' "
            END IF
            IF lvoid_man = 'V' THEN 
               LET batch_type = " and detdist.void_man = 'V' "
            END IF
           { IF lvoid_man = 'R' THEN 
                LET query_text = " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Deduction' ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_Run, detdist.pay_date ",
                                 " AS Pay_Date, LTRIM(RTRIM(detdist.acct)) ",
                                 " AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 END )) ",
                                 " AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Liability' ",
                                 " AS Acct_Type  from employee, detdist,  (select distinct fam_prof.yr ",
                                 " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type = 'D' ",
                                 batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " union all ",
                                 " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_run, detdist.pay_date ",
                                 " AS Pay_date, LTRIM(RTRIM(detdist.offset)) ",
                                 " AS Acct, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Liability' AS Liability ",
                                 " from employee, detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.offset)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) ",
                                 " union ALL ",
                                 "SELECT empl_no, f_name,m_name,l_name, Rec_type,Pay_run,Pay_date,Account,Pay_Fund_Account, CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) THEN  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END) ELSE cashqry.Credit END AS Credit, CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN  (CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) THEN ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END) ",
                                 " ELSE cashqry.Debit END AS Debit,  Acct_Type FROM ",
                                 " (select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end  ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_run, detdist.pay_date ",
                                 " AS Pay_date, LTRIM(RTRIM(detdist.acct)) AS Account, LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Credit, 'Expenditure' AS Acct_Type ",
                                 " from  employee,  detdist, (select distinct fam_prof.yr ",
                                 " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('O', 'F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct))) cashqry ",
                                 " group by empl_no, f_name,m_name,l_name,Rec_type,Pay_run,Pay_date,Account,Pay_Fund_Account,Debit,Credit,Acct_Type",
                                 " union all ",
                                 " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end ",
                                 " AS Rec_Type, detdist.pay_run AS Pay_run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, ",
                                 " case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                 " else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end  AS Pay_Fund_Account, ",
                                 " ABS(sum(case  when detdist.rec_type = 'O' and detdist.amount < 0 then detdist.amount when detdist.rec_type <> 'O' and detdist.amount > 0 ",
                                 " then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.rec_type = 'O' and detdist.amount > 0 then detdist.amount ",
                                 " when detdist.rec_type <> 'O' and detdist.amount < 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_type ",
                                 " from  employee, detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from  fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('O', 'F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name,  case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end ",
                                 " union all ",
                                 " SELECT empl_no,f_name,m_name,l_name,Rec_Type,Pay_Run,Pay_Date,faaccount,Pay_Fund_Account, ",
                                 " CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) then  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END AS Credit, ",
                                 " CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) then ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END AS Debit, ",
                                 " Acct_type FROM ",
                                 " (select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Deduction' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, 'Cash' AS Acct_type ",
                                 " from employee,  detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from  fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type = 'D' ",
                                 batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash))) cashqry ",
                                 " union ALL ",
                                 " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_Type ",
                                 " from employee, detdist,",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                  batch_type, " and substring(detdist.orgn_proj,1,4) in (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund from fam_prof) and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                 " union ALL ",
                                 " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Rec_type ",
                                 " from employee,  detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                 batch_type, " and substring(detdist.orgn_proj,1,4)  NOT  in ",
                                 " (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund ",
                                 " from  fam_prof) and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) "

            END IF 
            
           }
           IF lvoid_man = 'V' OR lvoid_man = 'M'  THEN
                LET query_text = " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Deduction' ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_Run, detdist.pay_date ",
                                 " AS Pay_Date, LTRIM(RTRIM(detdist.acct)) ",
                                 " AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 END )) ",
                                 " AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Liability' ",
                                 " AS Acct_Type  from employee, detdist,  (select distinct fam_prof.yr ",
                                 " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type = 'D' ",
                                 batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " union all ",
                                 " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_run, detdist.pay_date ",
                                 " AS Pay_date, LTRIM(RTRIM(detdist.offset)) ",
                                 " AS Acct, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Liability' AS Liability ",
                                 " from employee, detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.offset)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) ",
                                 " union ALL ",
                                 " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end  ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_run, detdist.pay_date ",
                                 " AS Pay_date, LTRIM(RTRIM(detdist.acct)) AS Acct, LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Credit, 'Expenditure' AS Acct_Type ",
                                 " from  employee,  detdist, (select distinct fam_prof.yr ",
                                 " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('O', 'F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " union all ",
                                 " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end ",
                                 " AS Rec_Type, detdist.pay_run AS Pay_run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, ",
                                 " case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                 " else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end  AS Pay_Fund_Account, ",
                                 " ABS(sum(case  when detdist.rec_type = 'O' and detdist.amount < 0 then detdist.amount when detdist.rec_type <> 'O' and detdist.amount > 0 ",
                                 " then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.rec_type = 'O' and detdist.amount > 0 then detdist.amount ",
                                 " when detdist.rec_type <> 'O' and detdist.amount < 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_type ",
                                 " from  employee, detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from  fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('O', 'F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name,  case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end ",
                                 " union all ",
                                 " SELECT empl_no,f_name,m_name,l_name,Rec_Type,Pay_Run,Pay_Date,faaccount,Pay_Fund_Account, ",
                                 " CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) then  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END AS Credit, ",
                                 " CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) then ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END AS Debit, ",
                                 " Acct_type FROM ",
                                 " (select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Deduction' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, 'Cash' AS Acct_type ",
                                 " from employee,  detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from  fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type = 'D' ",
                                 batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash))) cashqry ",
                                 " union ALL ",
                                 " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_Type ",
                                 " from employee, detdist,",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                  batch_type, " and substring(detdist.orgn_proj,1,4) in (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund from fam_prof) and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                 " union ALL ",
                                 " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Rec_type ",
                                 " from employee,  detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                 batch_type, " and substring(detdist.orgn_proj,1,4)  NOT  in ",
                                 " (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund ",
                                 " from  fam_prof) and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) "
            END IF  

           IF lvoid_man = 'P' THEN 
            LET query_text =" select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Deduction' ",
                         " AS Rec_type, detdist.pay_run ",
                         " AS Pay_Run, detdist.pay_date ",
                         " AS Pay_Date, LTRIM(RTRIM(detdist.acct)) ",
                         " AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                         " AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 END ) ",
                         " AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Liability' ",
                         " AS Acct_Type  from employee, detdist,  (select distinct fam_prof.yr ",
                         " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                         " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " AND employee.home_orgn like '%' ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') ",
                         " and detdist.rec_type = 'D' and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                         " LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                         " union all ",
                         " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' ",
                         " AS Rec_type, detdist.pay_run ",
                         " AS Pay_run, detdist.pay_date ",
                         " AS Pay_date, LTRIM(RTRIM(detdist.offset)) ",
                         " AS Acct, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Liability' AS Liability ",
                         " from employee, detdist, ",
                         " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') and detdist.rec_type in ('F', 'W') and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                         " LTRIM(RTRIM(detdist.offset)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) ",
                         " union ALL ",
                         " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end  ",
                         " AS Rec_type, detdist.pay_run ",
                         " AS Pay_run, detdist.pay_date ",
                         " AS Pay_date, LTRIM(RTRIM(detdist.acct)) AS Acct, LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) AS Pay_Fund_Account, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Credit, 'Expenditure' AS Acct_Type ",
                         " from  employee,  detdist, (select distinct fam_prof.yr ",
                         " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                         " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') and detdist.rec_type in ('O', 'F', 'W') and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                         " union all ",
                         " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end ",
                         " AS Rec_Type, detdist.pay_run AS Pay_run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, ",
                         " case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                         " else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end  AS Pay_Fund_Account, ",
                         " sum(case  when detdist.rec_type = 'O' and detdist.amount < 0 then detdist.amount when detdist.rec_type <> 'O' and detdist.amount > 0 ",
                         " then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.rec_type = 'O' and detdist.amount > 0 then detdist.amount ",
                         " when detdist.rec_type <> 'O' and detdist.amount < 0 then detdist.amount else 0 end ) AS Credit, 'Cash' AS Acct_type ",
                         " from  employee, detdist, ",
                         " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                         " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from  fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') and detdist.rec_type in ('O', 'F', 'W') and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name,  case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end ",
                         " union all ",
                         " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Deduction' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Cedit, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, 'Cash' AS Acct_type ",
                         " from employee,  detdist, ",
                         " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from  fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') and detdist.rec_type = 'D' and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                         " union ALL ",
                         " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Cash' AS Acct_Type ",
                         " from employee, detdist,",
                         " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') and detdist.rec_type in ('F', 'W') and substring(detdist.orgn_proj,1,4) in (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund from fam_prof) and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                         " union ALL ",
                         " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Cash' AS Rec_type ",
                         " from employee,  detdist, ",
                         " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                         " from fam_prof) Payroll_Cash_Account ",
                         " where ", where_part,
                         " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                         " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                         lemployee_text ,
                         " and detdist.void_man = '' and detdist.redist in ('', 'F', 'P', 'S') and detdist.rec_type in ('F', 'W') and substring(detdist.orgn_proj,1,4)  NOT  in ",
                         " (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund ",
                         " from  fam_prof) and employee.empl_no = detdist.empl_no ",
                         " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                         " UNION ALL ",
                         " select distinct '' AS empl_no , '' AS f_name , '' as m_name,'' AS l_name,Accrued_Net_w_Titles.Rec_type As Rec_type,Accrued_Net_w_Titles.Journal_Entry_Title AS Pay_Run, Accrued_Net_w_Titles.Transaction_Date AS Pay_Date,Accrued_Net_w_Titles.Account_Code as faaccount, Accrued_Net_w_Titles.Account_Number AS Pay_Fund_Account, Accrued_Net_w_Titles.Debit AS Debit,Accrued_Net_w_Titles.Credit AS Credit,Accrued_Net_w_Titles.Type AS Acct_type ",
                         " from (select distinct Accrued_Net_Pay_Trans.Journal_Entry_Title AS Journal_Entry_Title,Accrued_Net_Pay_Trans.Transaction_Date AS Transaction_Date, Accrued_Net_Pay_Trans.Account_Code AS Account_Code, Accrued_Net_Pay_Trans.Account_Number AS Account_Number,Account_Titles.GL_Account_Title AS GL_Account_Title,Accrued_Net_Pay_Trans.Type AS Type, Accrued_Net_Pay_Trans.Debit AS Debit,Accrued_Net_Pay_Trans.Credit AS Credit,Accrued_Net_Pay_Trans.Rec_type As Rec_type ",
                         " from (select distinct Union4.Journal_Entry_Title AS Journal_Entry_Title, Union4.Transaction_Date AS Transaction_Date,Union4.Account_Code AS Account_Code, Union4.Account_Number AS Account_Number, Union4.Type AS Type, Union4.Debit AS Debit, Union4.Credit AS Credit,Union4.Rec_type As Rec_type ",		
                         " from (select T0.C0 AS Journal_Entry_Title,T0.C1 AS Transaction_Date, T0.C2 AS Account_Code, T0.C3 AS Account_Number, T0.C4 AS Type,T0.C5 AS Debit, T0.C6 AS Credit,T0.C7 AS Rec_type ",  
                         " from (select LTRIM(RTRIM(transact.je_desc)) AS C0, transact.trans_date AS C1, LTRIM(RTRIM(transact_account.acct)) AS C2,LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund)) +' - '+ LTRIM(RTRIM(transact_account.acct)) AS C3, 'Liability' AS C4, ",
                         " sum(case  when transact.trans_amt < 0 then abs(transact.trans_amt) else 0 end ) AS C5, sum(case  when transact.trans_amt > 0 then transact.trans_amt else 0 end ) AS C6,transact.description AS C7 from  transact ",
                         " LEFT OUTER JOIN faaccount transact_account on transact.account = transact_account.acct, ",
                         " (select distinct fam_prof.yr AS Yr,LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund,case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash))else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash from  fam_prof) Payroll_Cash_Account ",
                         " where LTRIM(RTRIM(transact.je_desc)) =  ", "'",r_paygroups.pay_run,"'",
                         " and transact_account.acct in ",
                         " (select distinct LTRIM(RTRIM(hrm_prof.np_acct)) AS Np_Acct from  hrm_prof) and LTRIM(RTRIM(transact.je_desc))  NOT  in (select distinct LTRIM(RTRIM(paygroups.pay_run)) AS Pay_Run from  paygroups where paygroups.proc_sumfisc = 'Y') ",
                         " group by LTRIM(RTRIM(transact.je_desc)), transact.trans_date, LTRIM(RTRIM(transact_account.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(transact_account.acct)), transact.description) T0 ",			
                         " UNION ALL ",	
                         " SELECT T0.C0 AS Journal_Entry_Title, T0.C1 AS Transaction_Date, T0.C2 AS Account_Code, T0.C3 AS Account_Number, T0.C4 AS Type, T0.C5 AS Debit,T0.C6 AS Credit,T0.C7 AS Rec_type ",
                         " FROM (SELECT LTRIM(RTRIM(transact.je_desc)) AS C0, transact.trans_date AS C1,LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS C2,LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS C3, 'Cash' AS C4, ",
                         " sum(case  when transact.trans_amt > 0 then transact.trans_amt else 0 end ) AS C5, sum(case  when transact.trans_amt < 0 then abs(transact.trans_amt) else 0 end ) AS C6,transact.description AS C7 ", 		
                         " FROM  transact LEFT OUTER JOIN faaccount transact_account on transact.account = transact_account.acct, ",
                         " (SELECT distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, CASE  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash FROM  fam_prof) Payroll_Cash_Account ",
                         " WHERE LTRIM(RTRIM(transact.je_desc)) =  ", "'",r_paygroups.pay_run,"'",
                         " AND transact_account.acct in ",
                         " (select distinct LTRIM(RTRIM(hrm_prof.np_acct)) AS Np_Acct from  hrm_prof) and LTRIM(RTRIM(transact.je_desc))  NOT  in (select distinct LTRIM(RTRIM(paygroups.pay_run)) AS Pay_Run  from  paygroups where paygroups.proc_sumfisc = 'Y') ",
                         " group by LTRIM(RTRIM(transact.je_desc)), transact.trans_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), transact.description) T0)  Union4(Journal_Entry_Title,  Transaction_Date,Account_Code,  Account_Number,Type, Debit, Credit, Rec_type)) Accrued_Net_Pay_Trans, ",
                         " (select distinct LTRIM(RTRIM(faaccount.acct)) AS GL_Account_Code,LTRIM(RTRIM(faaccount.title)) AS GL_Account_Title from faaccount where LTRIM(RTRIM(faaccount.acct))  NOT BETWEEN '", lfamlowequ.trim() ,"' and '", lfamhighequ.trim() ,"' and  LTRIM(RTRIM(faaccount.acct))  NOT BETWEEN '", lfamlowrev.trim() ,"' and '", lfamhighrev.trim() ,"' ) Account_Titles ",
                         " where Accrued_Net_Pay_Trans.Account_Code = Account_Titles.GL_Account_Code) Accrued_Net_w_Titles, ",
                         " (select distinct T1.C0 AS Pay_Run, T1.C1 AS Run_Desc, T1.C2 AS Display_Value, T0.C1 AS End_Date FROM ",
                         " (select paygroups.pay_run AS C0,min(paygroups.end_date) AS C1 from  paygroups group by paygroups.pay_run) T0, (select paygroups.pay_run AS C0, paygroups.run_desc AS C1, ", 
                         " LTRIM(RTRIM(paygroups.pay_run)) + ' - ' + LTRIM(RTRIM(paygroups.run_desc)) AS C2 FROM  paygroups) T1 WHERE T1.C0 = T0.C0 or T1.C0 is null and T0.C0 is null) Prompt_Pay_Run_Description ",
                         " WHERE Accrued_Net_w_Titles.Journal_Entry_Title = Prompt_Pay_Run_Description.Pay_Run ",
                         " order by Pay_Fund_Account "
            END IF

             IF lvoid_man = 'R' THEN
                LET query_text = " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name,'Deduction' ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_Run, detdist.pay_date ",
                                 " AS Pay_Date, LTRIM(RTRIM(detdist.acct)) ",
                                 " AS Account, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 END )) ",
                                 " AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Liability' ",
                                 " AS Acct_Type  from employee, detdist,  (select distinct fam_prof.yr ",
                                 " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type = 'D' ",
                                 batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                 " union all ",
                                 " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' ",
                                 " AS Rec_type, detdist.pay_run ",
                                 " AS Pay_run, detdist.pay_date ",
                                 " AS Pay_date, LTRIM(RTRIM(detdist.offset)) ",
                                 " AS Acct, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Liability' AS Liability ",
                                 " from employee, detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.offset)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) ",
                                 " union ALL ",
                                 " SELECT empl_no,f_name,m_name,l_name, Rec_type,Pay_run,Pay_date,Account,Pay_Fund_Account,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) THEN ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END) ELSE cashqry.Debit END AS Debit,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) THEN  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END) ELSE cashqry.Credit END AS Credit, Acct_Type FROM ",
                                 " (select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end   AS Rec_type, detdist.pay_run  AS Pay_run,",
                                 " detdist.pay_date  AS Pay_date, LTRIM(RTRIM(detdist.acct)) AS Account, LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Debit,",
                                 " ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Credit, 'Expenditure' AS Acct_Type  from  employee,  detdist, (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund,",
                                 " case  when fam_prof.pyrl_venpay_cash = 'Y'  then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash  from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('O', 'F', 'W') ",
                                 batch_type, 
                                 " and employee.empl_no = detdist.empl_no group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, ",
                                 " LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct))) cashqry ",
                                 " union all ",
                                 " SELECT empl_no,f_name,m_name,l_name, Rec_type,Pay_run,Pay_Date,Account,Pay_Fund_Account, ",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) THEN ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END) ELSE cashqry.Debit END AS Debit,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) THEN  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END) ELSE cashqry.Credit END AS Credit,",
                                 " Acct_type FROM ",
                                 " (select employee.empl_no,employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end ",
                                 " AS Rec_type, detdist.pay_run AS Pay_run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Account, ",
                                 " case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                 " else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end  AS Pay_Fund_Account, ",
                                 " ABS(sum(case  when detdist.rec_type = 'O' and detdist.amount < 0 then detdist.amount when detdist.rec_type <> 'O' and detdist.amount > 0 ",
                                 " then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.rec_type = 'O' and detdist.amount > 0 then detdist.amount ",
                                 " when detdist.rec_type <> 'O' and detdist.amount < 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_type ",
                                 " from  employee, detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                 " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from  fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('O', 'F', 'W') ",
                                  batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end ) cashqry ",
                                 " union all ",
                                 " SELECT empl_no,f_name,m_name,l_name, Rec_Type,Pay_Run,Pay_Date,Account,Pay_Fund_Account,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) THEN ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END) ELSE cashqry.Debit END AS Debit,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) THEN  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END) ELSE cashqry.Credit END AS Credit,",
                                 " Acct_type FROM ",
                                 " (select employee.empl_no,employee.f_name,employee.m_name,employee.l_name, 'Deduction' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Account, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, 'Cash' AS Acct_type ",
                                 " from employee,  detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from  fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type = 'D' ",
                                 batch_type, " and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash))) cashqry ",
                                 " union ALL ",
                                 " SELECT empl_no,f_name,m_name,l_name, Rec_Type,Pay_Run,Pay_Date,Account,Pay_Fund_Account,  ",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) THEN ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END) ELSE cashqry.Debit END AS Debit,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) THEN  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END) ELSE cashqry.Credit END AS Credit,",
                                 " Acct_Type FROM ",
                                 " (select employee.empl_no,employee.f_name,employee.m_name,employee.l_name,'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Account, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_Type ",
                                 " from employee, detdist,",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                  batch_type, " and substring(detdist.orgn_proj,1,4) in (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund from fam_prof) and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash))) cashqry ",
                                 " union ALL ",
                                  " SELECT empl_no,f_name,m_name,l_name, Rec_Type,Pay_Run,Pay_Date,Account,Pay_Fund_Account,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) THEN ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END) ELSE cashqry.Debit END AS Debit,",
                                 " CASE WHEN ABS(cashqry.Debit) <> 0 AND ABS(cashqry.Credit) <> 0 THEN (CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) THEN  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END) ELSE cashqry.Credit END AS Credit,",
                                 " Acct_Type FROM ",
                                 " (select employee.empl_no,employee.f_name,employee.m_name,employee.l_name,'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Account, substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, ABS(sum(case  when detdist.amount < 0 then detdist.amount else 0 end )) AS Debit, ABS(sum(case  when detdist.amount > 0 then detdist.amount else 0 end )) AS Credit, 'Cash' AS Acct_Type ",
                                 " from employee,  detdist, ",
                                 " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                 " from fam_prof) Payroll_Cash_Account ",
                                 " where ", where_part,
                                 " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                 " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                 lemployee_text ,
                                 " and detdist.rec_type in ('F', 'W') ",
                                 batch_type, " and substring(detdist.orgn_proj,1,4)  NOT  in ",
                                 " (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund ",
                                 " from  fam_prof) and employee.empl_no = detdist.empl_no ",
                                 " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash))) cashqry "
                        END IF 
            
            IF lvoid_man = 'P' THEN
               LET progress_text1 = " UNION ALL ",
                                   " select distinct '' AS empl_no , '' AS f_name , '' as m_name,'' AS l_name,Accrued_Net_w_Titles.Rec_type As Rec_type,Accrued_Net_w_Titles.Journal_Entry_Title AS Pay_Run, Accrued_Net_w_Titles.Transaction_Date AS Pay_Date,Accrued_Net_w_Titles.Account_Code as faaccount, Accrued_Net_w_Titles.Account_Number AS Pay_Fund_Account, Accrued_Net_w_Titles.Debit AS Debit,Accrued_Net_w_Titles.Credit AS Credit,Accrued_Net_w_Titles.Type AS Acct_type ",
                                   " from (select distinct Accrued_Net_Pay_Trans.Journal_Entry_Title AS Journal_Entry_Title,Accrued_Net_Pay_Trans.Transaction_Date AS Transaction_Date, Accrued_Net_Pay_Trans.Account_Code AS Account_Code, Accrued_Net_Pay_Trans.Account_Number AS Account_Number,Account_Titles.GL_Account_Title AS GL_Account_Title,Accrued_Net_Pay_Trans.Type AS Type, Accrued_Net_Pay_Trans.Debit AS Debit,Accrued_Net_Pay_Trans.Credit AS Credit,Accrued_Net_Pay_Trans.Rec_type As Rec_type ",
                                   " from (select distinct Union4.Journal_Entry_Title AS Journal_Entry_Title, Union4.Transaction_Date AS Transaction_Date,Union4.Account_Code AS Account_Code, Union4.Account_Number AS Account_Number, Union4.Type AS Type, Union4.Debit AS Debit, Union4.Credit AS Credit,Union4.Rec_type As Rec_type ",		
                                   " from (select T0.C0 AS Journal_Entry_Title,T0.C1 AS Transaction_Date, T0.C2 AS Account_Code, T0.C3 AS Account_Number, T0.C4 AS Type,T0.C5 AS Debit, T0.C6 AS Credit,T0.C7 AS Rec_type ",  
                                   " from (select LTRIM(RTRIM(transact.je_desc)) AS C0, transact.trans_date AS C1, LTRIM(RTRIM(transact_account.acct)) AS C2,LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund)) +' - '+ LTRIM(RTRIM(transact_account.acct)) AS C3, 'Liability' AS C4, ",
                                   " sum(case  when transact.trans_amt < 0 then abs(transact.trans_amt) else 0 end ) AS C5, sum(case  when transact.trans_amt > 0 then transact.trans_amt else 0 end ) AS C6,transact.description AS C7 from  transact ",
                                   " LEFT OUTER JOIN faaccount transact_account on transact.account = transact_account.acct, ",
                                   " (select distinct fam_prof.yr AS Yr,LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund,case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash))else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash from  fam_prof) Payroll_Cash_Account ",
                                   " where LTRIM(RTRIM(transact.je_desc)) =  ", "'",r_paygroups.pay_run,"'",
                                   " and transact_account.acct in ",
                                   " (select distinct LTRIM(RTRIM(hrm_prof.np_acct)) AS Np_Acct from  hrm_prof) and LTRIM(RTRIM(transact.je_desc))  NOT  in (select distinct LTRIM(RTRIM(paygroups.pay_run)) AS Pay_Run from  paygroups where paygroups.proc_sumfisc = 'Y') ",
                                   " group by LTRIM(RTRIM(transact.je_desc)), transact.trans_date, LTRIM(RTRIM(transact_account.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(transact_account.acct)), transact.description) T0 ",			
                                   " UNION ALL ",	
                                   " SELECT T0.C0 AS Journal_Entry_Title, T0.C1 AS Transaction_Date, T0.C2 AS Account_Code, T0.C3 AS Account_Number, T0.C4 AS Type, T0.C5 AS Debit,T0.C6 AS Credit,T0.C7 AS Rec_type ",
                                   " FROM (SELECT LTRIM(RTRIM(transact.je_desc)) AS C0, transact.trans_date AS C1,LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS C2,LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS C3, 'Cash' AS C4, ",
                                   " sum(case  when transact.trans_amt > 0 then transact.trans_amt else 0 end ) AS C5, sum(case  when transact.trans_amt < 0 then abs(transact.trans_amt) else 0 end ) AS C6,transact.description AS C7 ", 		
                                   " FROM  transact LEFT OUTER JOIN faaccount transact_account on transact.account = transact_account.acct, ",
                                   " (SELECT distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, CASE  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash FROM  fam_prof) Payroll_Cash_Account ",
                                   " WHERE LTRIM(RTRIM(transact.je_desc)) =  ", "'",r_paygroups.pay_run,"'",
                                   " AND transact_account.acct in ",
                                   " (select distinct LTRIM(RTRIM(hrm_prof.np_acct)) AS Np_Acct from  hrm_prof) and LTRIM(RTRIM(transact.je_desc))  NOT  in (select distinct LTRIM(RTRIM(paygroups.pay_run)) AS Pay_Run  from  paygroups where paygroups.proc_sumfisc = 'Y') ",
                                   " group by LTRIM(RTRIM(transact.je_desc)), transact.trans_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), transact.description) T0)  Union4(Journal_Entry_Title,  Transaction_Date,Account_Code,  Account_Number,Type, Debit, Credit, Rec_type)) Accrued_Net_Pay_Trans, ",
                                   " (select distinct LTRIM(RTRIM(faaccount.acct)) AS GL_Account_Code,LTRIM(RTRIM(faaccount.title)) AS GL_Account_Title from faaccount where LTRIM(RTRIM(faaccount.acct))  NOT BETWEEN '", lfamlowequ.trim() ,"' and '", lfamhighequ.trim() ,"' and  LTRIM(RTRIM(faaccount.acct))  NOT BETWEEN '", lfamlowrev.trim() ,"' and '", lfamhighrev.trim() ,"' ) Account_Titles ",
                                   " where Accrued_Net_Pay_Trans.Account_Code = Account_Titles.GL_Account_Code) Accrued_Net_w_Titles, ",
                                   " (select distinct T1.C0 AS Pay_Run, T1.C1 AS Run_Desc, T1.C2 AS Display_Value, T0.C1 AS End_Date FROM ",
                                   " (select paygroups.pay_run AS C0,min(paygroups.end_date) AS C1 from  paygroups group by paygroups.pay_run) T0, (select paygroups.pay_run AS C0, paygroups.run_desc AS C1, ", 
                                   " LTRIM(RTRIM(paygroups.pay_run)) + ' - ' + LTRIM(RTRIM(paygroups.run_desc)) AS C2 FROM  paygroups) T1 WHERE T1.C0 = T0.C0 or T1.C0 is null and T0.C0 is null) Prompt_Pay_Run_Description ",
                                   " WHERE Accrued_Net_w_Titles.Journal_Entry_Title = Prompt_Pay_Run_Description.Pay_Run "

                     ELSE 
                     LET progress_text1 = ""
               END IF                                           #EFIN-69331 <--
                           
               LET progress_text = " select count(*) from ( ",
                                   " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Deduction' ",
                                   " AS Rec_type, detdist.pay_run ",
                                   " AS Pay_Run, detdist.pay_date ",
                                   " AS Pay_Date, LTRIM(RTRIM(detdist.acct)) ",
                                   " AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                   " AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 END ) ",
                                   " AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Liability' ",
                                   " AS Acct_Type  from employee, detdist,  (select distinct fam_prof.yr ",
                                   " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                   " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " AND employee.home_orgn like '%' ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type = 'D' ", 
                                    batch_type, " and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                   " LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                   " union all ",
                                   " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' ",
                                   " AS Rec_type, detdist.pay_run ",
                                   " AS Pay_run, detdist.pay_date ",
                                   " AS Pay_date, LTRIM(RTRIM(detdist.offset)) ",
                                   " AS Acct, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Liability' AS Liability ",
                                   " from employee, detdist, ",
                                   " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type in ('F', 'W') ", 
                                    batch_type, " and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, ",
                                   " LTRIM(RTRIM(detdist.offset)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(detdist.offset)) ",
                                   " union ALL ",
                                   " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end  ",
                                   " AS Rec_type, detdist.pay_run ",
                                   " AS Pay_run, detdist.pay_date ",
                                   " AS Pay_date, LTRIM(RTRIM(detdist.acct)) AS Acct, LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) AS Pay_Fund_Account, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Credit, 'Expenditure' AS Acct_Type ",
                                   " from  employee,  detdist, (select distinct fam_prof.yr ",
                                   " AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                   " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " And employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type in ('O', 'F', 'W') ",
                                    batch_type, " and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(detdist.acct)), LTRIM(RTRIM(detdist.orgn_proj))+' - '+LTRIM(RTRIM(detdist.acct)) ",
                                   " union all ",
                                   " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end ",
                                   " AS Rec_Type, detdist.pay_run AS Pay_run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, ",
                                   " case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                   " else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end  AS Pay_Fund_Account, ",
                                   " sum(case  when detdist.rec_type = 'O' and detdist.amount < 0 then detdist.amount when detdist.rec_type <> 'O' and detdist.amount > 0 ",
                                   " then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.rec_type = 'O' and detdist.amount > 0 then detdist.amount ",
                                   " when detdist.rec_type <> 'O' and detdist.amount < 0 then detdist.amount else 0 end ) AS Credit, 'Cash' AS Acct_type ",
                                   " from  employee, detdist, ",
                                   " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' ",
                                   " then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from  fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type in ('O', 'F', 'W') ",
                                   batch_type, " and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name,  case  when detdist.rec_type = 'O' then 'Salary' else 'Fringe' end , detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), case  when detdist.rec_type = 'O' then LTRIM(RTRIM(substring(detdist.orgn_proj,1,4)))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) else LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) end ",
                                   " union all ",
                                   " SELECT empl_no,f_name,m_name,l_name,Rec_Type,Pay_Run,Pay_Date,faaccount,Pay_Fund_Account, ",
                                   " CASE WHEN ABS(cashqry.Credit) > ABS(cashqry.Debit) then  ABS(cashqry.Credit) - ABS(cashqry.Debit) ELSE 0 END AS Credit, ",
                                   " CASE WHEN ABS(cashqry.Debit) >= ABS(cashqry.Credit) then ABS(cashqry.Debit) - ABS(cashqry.Credit) ELSE 0 END AS Debit, ",
                                   " Acct_type FROM ",
                                   " (select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Deduction' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, 'Cash' AS Acct_type ",
                                   " from employee,  detdist, ",
                                   " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from  fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type = 'D' ",
                                   batch_type, " and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash))) cashqry ",
                                   " union ALL ",
                                   " select employee.empl_no AS empl_no,employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Cash' AS Acct_Type ",
                                   " from employee, detdist,",
                                   " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " and employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type in ('F', 'W') ",
                                   batch_type, "  and substring(detdist.orgn_proj,1,4) in (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund from fam_prof) and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no, employee.f_name,employee.m_name,employee.l_name,detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), LTRIM(RTRIM(Payroll_Cash_Account.Pay_Fund))+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ",
                                   " union ALL ",
                                   " select employee.empl_no AS empl_no, employee.f_name,employee.m_name,employee.l_name, 'Fringe' AS Rec_Type, detdist.pay_run AS Pay_Run, detdist.pay_date AS Pay_Date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS faaccount, substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) AS Pay_Fund_Account, sum(case  when detdist.amount < 0 then detdist.amount else 0 end ) AS Debit, sum(case  when detdist.amount > 0 then detdist.amount else 0 end ) AS Credit, 'Cash' AS Rec_type ",
                                   " from employee,  detdist, ",
                                   " (select distinct fam_prof.yr AS Yr, LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund, case  when fam_prof.pyrl_venpay_cash = 'Y' then LTRIM(RTRIM(fam_prof.pay_cash)) else LTRIM(RTRIM(fam_prof.cash_account)) end  AS Pay_Cash ",
                                   " from fam_prof) Payroll_Cash_Account ",
                                   " where ", where_part,
                                   " AND employee.home_orgn like '%' and employee.base_loc >= 0 ",
                                   " and detdist.pay_run  = ", "'",r_paygroups.pay_run,"'",
                                   lemployee_text ,
                                   " and detdist.rec_type in ('F', 'W') ",
                                    batch_type, " and substring(detdist.orgn_proj,1,4)  NOT  in ",
                                   " (select distinct LTRIM(RTRIM(fam_prof.pay_fund)) AS Pay_Fund ",
                                   " from  fam_prof) and employee.empl_no = detdist.empl_no ",
                                   " group by employee.empl_no,employee.f_name,employee.m_name,employee.l_name, detdist.pay_run, detdist.pay_date, LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)), substring(detdist.orgn_proj,1,4)+' - '+LTRIM(RTRIM(Payroll_Cash_Account.Pay_Cash)) ", progress_text1, " ) temp "


      PREPARE cor2 FROM progress_text
      DECLARE corcr2 CURSOR FOR cor2
      LET progress_total = 0
      OPEN corcr2
      FETCH corcr2 INTO progress_total
      CLOSE corcr2

      PREPARE ocr1 FROM query_text
      DECLARE curs1 CURSOR FOR ocr1

      IF NOT rpt_setup(6, "payrollregister.rpt") THEN
         LET int_flag = FALSE
         CONTINUE WHILE
      END IF

      CALL libProgressBarStartCancel(
         "Processing...", progress_total, TRUE)
         RETURNING lProcStatus, lMessage, mWindowOpened

      LET info1 = ""

       CALL report_hdr(132," PAYROLL REGISTER FOR PAY RUN ","PAYROLLREGISTER1",info1,
                           lhdr_where_part,r_hrm_prof.company,r_hrm_prof.system,
                            r_hrm_prof.client) RETURNING r_hdr.*

      CASE
         WHEN p_rpt_dest = "t" OR p_rpt_dest = "d"
            START REPORT payrollreg_rpt TO p_rpt_name
            CALL payroll_register_4rp(mDataOption)
         WHEN p_rpt_dest = "p"
            START REPORT payrollreg_rpt TO PIPE p_prt_cmd
            CALL payroll_register_4rp(mDataOption)

         WHEN  p_rpt_dest = "x"
                  START REPORT payrollreg_rpt TO XML HANDLER
                         om.XmlWriter.createFileWriter(p_rpt_name)

      END CASE

      LET col1 = (132 - (LENGTH(r_hrm_prof.company) + 13 + LENGTH(r_hrm_prof.system)))
      LET col2 = (132 - LENGTH(r_hrm_prof.client)) / 2

      MESSAGE "Printing Payroll Register Report"
      IF mWindowOpened = TRUE THEN
         LET user_inter = FALSE
         FOREACH curs1
            INTO r_payroll.*
         SELECT run_desc INTO lrun_desc FROM paygroups WHERE paygroups.pay_run = r_payroll.pay_run  AND group_x = '#'
         SELECT title INTO ltitle FROM faaccount WHERE  faaccount.acct = r_payroll.acct
            CALL libProgressBarProcess()


                        #EFIN-69331 <--
       
         
           IF r_payroll.Debit <> 0 AND  r_payroll.Credit <> 0 THEN
               LET r_payroll.Debit = 0
               LET r_payroll.Credit = 0 
            END IF      
           
            IF p_rpt_dest = "t" OR p_rpt_dest = "d" OR  p_rpt_dest = "x" THEN
               IF r_payroll.Debit = 0 AND r_payroll.Credit = 0 THEN #EFIN-69331 --
                  ELSE
                  OUTPUT TO REPORT payrollreg_rpt(r_payroll.*,lrun_desc,ltitle)
               END IF                                           #EFIN-69331 --
               LET row_id = row_id + 1
            END IF
            IF int_flag THEN
               LET int_flag = FALSE
               LET user_inter = TRUE
               EXIT FOREACH
            END IF

         END FOREACH

         IF p_rpt_dest = "t" OR p_rpt_dest = "d" OR  p_rpt_dest = "x" THEN
            FINISH REPORT payrollreg_rpt

         END IF
         CALL libProgressBarFinish(TRUE)

         IF user_inter THEN
            ERROR "Report execution interrupted"
         ELSE
            ## Call viewer if terminal option was chosen.
            IF p_rpt_dest = "t" AND p_rpt_dest = "d" THEN
               LET run_str ="sh -c \"$menx/use_132.sh ",p_rpt_name CLIPPED," ",use_132_mode(),"\""
               ##XRUN run_str WITHOUT WAITING
               CALL libReportFileToPDF(
                  p_rpt_name, TRUE, TRUE, TRUE)
                  RETURNING lProcStatus, lMessage
            END IF

            IF p_rpt_dest = 'x' THEN
                  CALL libExcelHandler(p_rpt_name, p_prt_cmd)
                     RETURNING lProcStatus
            END IF
            ERROR "Report Completed"
         END IF
      ELSE # lWindowOpened
         CALL libErrDialog("Attention", lMessage, "exclaim", "Ok")
      END IF # lWindowOpened
   END WHILE # reenter

   CLOSE WINDOW payrollregister

END FUNCTION #payrollregister

FUNCTION LoadEmployeeNoCombo(pComboBox ui.Combobox) RETURNS ()
      DEFINE lCode LIKE employee.empl_no
      DEFINE query_text_load STRING

      LET pComboBox = NVL(pComboBox, ui.ComboBox.forName("formonly.empl_no"))

       LET query_text_load = " SELECT distinct detdist.empl_no ",
                             " FROM  detdist ,employee ",
                             " WHERE detdist.pay_run =  ", "'",r_paygroups.pay_run,"'",
                             " and detdist.empl_no = employee.empl_no "


       PREPARE p_empl_curs FROM query_text_load
       DECLARE cursEmpl CURSOR FOR p_empl_curs
           CALL pComboBox.clear()
       FOREACH cursEmpl INTO lCode
           CALL pComboBox.addItem(lCode CLIPPED,lCode CLIPPED)
       END FOREACH

END FUNCTION #LoadEmployeeNoCombo

FUNCTION LoadPaydateCombo(pComboBox ui.Combobox) RETURNS ()
      DEFINE lCode LIKE detdist.pay_date
      DEFINE empl_text               STRING
      DEFINE query_text_Paydate_load STRING

      LET pComboBox = ui.ComboBox.forName("detdist.pay_date")

      IF r_employee.empl_no IS NOT NULL THEN
         IF r_employee.empl_no = 0 THEN
            ELSE
            LET empl_text = " and detdist.empl_no = ", "'",r_employee.empl_no,"'"
         END IF
      END IF

      LET query_text_Paydate_load = " SELECT distinct detdist.pay_date ",
                                    " FROM  detdist,employee ",
                                    " WHERE detdist.pay_run =  ", "'",r_paygroups.pay_run,"'", empl_text

       PREPARE p_paydate_curs FROM query_text_Paydate_load
       DECLARE cursPaydate CURSOR FOR p_paydate_curs
           CALL pComboBox.clear()
       FOREACH cursPaydate INTO lCode
           CALL pComboBox.addItem(lCode CLIPPED,lCode CLIPPED)
       END FOREACH

END FUNCTION #LoadPaydateCombo

#Ascii base text report
REPORT payrollreg_rpt(r_payroll,lrun_desc,ltitle)

   DEFINE r_payroll RECORD
          empl_no   LIKE detdist.empl_no,
          f_name    LIKE employee.f_name,
          m_name    LIKE employee.m_name,
          l_name    LIKE employee.l_name,
          rec_type  STRING,
          pay_run   LIKE detdist.pay_run,
          pay_date  LIKE detdist.pay_date,
          acct      LIKE detdist.acct,
          Pay_Fund_Account STRING ,
          Debit            DECIMAL(12,2),
          Credit           DECIMAL(12,2)
   END RECORD
   DEFINE lrun_desc     STRING
   DEFINE ltitle        STRING
   DEFINE r_transact    RECORD LIKE transact.*
   DEFINE lvoid_man     STRING                                  #EFIN-69331

   OUTPUT
      LEFT MARGIN 3
      TOP MARGIN 1
      BOTTOM MARGIN 1
      PAGE LENGTH 66

   FORMAT
       FIRST PAGE HEADER
         LET l_debit = 0.00
         LET l_credit = 0.00
         LET grd_total_debit = 0.00
         LET grd_total_credit = 0.00

         PRINT r_hdr.line1 CLIPPED,COLUMN r_hdr.pagecol,(PAGENO USING r_hdr.page) CLIPPED
         PRINT r_hdr.line2 CLIPPED
         PRINT r_hdr.line3 CLIPPED

         SELECT * INTO r_faorgn.*
         FROM orgn
         WHERE orgn.key_orgn = r_transact.fund
         AND orgn.yr = r_transact.yr

         LET pay_run_desc = "PAYROLL REGISTER FOR PAY RUN",r_payroll.pay_run  CLIPPED, " ", " - ", " ", lrun_desc

         LET lvoid_man = r_detdist.void_man                     #EFIN-69331 -->

          IF p_rpt_dest = 'x' THEN
             IF lvoid_man = 'P'  THEN
                LET lvoid_man = 'PAYRUN'
             END IF

            IF lvoid_man = 'V' THEN
                LET lvoid_man = 'VOID'
            END IF

            IF lvoid_man = 'M' THEN
                LET lvoid_man = 'MANUAL'
            END IF

            IF lvoid_man = 'R' THEN
                LET lvoid_man = 'REDISTRIBUTION'
            END IF
        ELSE 
         IF lvoid_man = 'P'  THEN
            LET lvoid_man = 'PAYRUN'
            LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF

         IF lvoid_man = 'V' THEN
          LET lvoid_man = 'VOID'
          LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF

         IF lvoid_man = 'M' THEN
            LET lvoid_man = 'MANUAL'
            LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF

         IF lvoid_man = 'R' THEN
            LET lvoid_man = 'REDISTRIBUTION'
            LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF
         END IF                                                 #EFIN-69331

         IF p_rpt_dest = "t" OR p_rpt_dest = "d" THEN
            LET col3 = ((132 - LENGTH(lvoid_man CLIPPED)) / 2 ) + 1
            PRINT COLUMN col3, lvoid_man CLIPPED
         ELSE
         PRINT ""
         END IF                                                 #EFIN-69331 <--

         INITIALIZE empl_no,empl_name TO NULL
         IF r_employee.empl_no IS NOT NULL THEN
               IF r_payroll.empl_no = 0 THEN
               ELSE
               LET empl_no = r_payroll.empl_no
               LET empl_no = "("||empl_no.trimLeftWhiteSpace()||")"
               LET empl_name = "EMPLOYEE : ",r_payroll.f_name CLIPPED, ",",
                      r_payroll.l_name CLIPPED, " ", 
                      r_payroll.m_name CLIPPED, empl_no CLIPPED
               END IF
         END IF

         SKIP 1 LINE

         IF p_rpt_dest = 'x' THEN
            PRINT COLUMN 1,  "BATCH TYPE",                      #EFIN-69331
                  COLUMN 2,  "EMPLOYEE",
                  COLUMN 3,  "PAY RUN",
                  COLUMN 4,  "PAYRUN DESC",
                  COLUMN 5,  "PAY DATE",
                  COLUMN 6,  "ACCOUNT NUMBER",
                  COLUMN 7,  "ACCOUNT TITLE",
                  COLUMN 8,  "REC TYPE",
                  COLUMN 9,  "DEBIT",
                  COLUMN 10, "CREDIT"
            PRINT ""
            PRINT ""
            PRINT ""
         ELSE
         IF empl_name = 0 THEN

            SKIP 1 LINE
            PRINT COLUMN 1,   "PAY RUN",
                  COLUMN 11,  "PAYRUN DESC",
                  COLUMN 26,  "PAY DATE",
                  COLUMN 42,  "ACCOUNT NUMBER/",
                  COLUMN 75,  "REC TYPE",
                  COLUMN 117, "DEBIT",
                  COLUMN 130, "CREDIT"
            PRINT COLUMN 42,  "ACCOUNT TITLE"
            PRINT  ""
         ELSE
            PRINT COLUMN 1,   empl_name
            SKIP 1 LINE
            PRINT COLUMN 1,   "PAY RUN",
                  COLUMN 11,  "PAYRUN DESC",
                  COLUMN 26,  "PAY DATE",
                  COLUMN 42,  "ACCOUNT NUMBER/",
                  COLUMN 75,  "REC TYPE",
                  COLUMN 117, "DEBIT",
                  COLUMN 130, "CREDIT"
            PRINT COLUMN 42,  "ACCOUNT TITLE"
            END IF
         END IF

         SKIP 1 LINE
      LET mDataOption = p_prt_cmd[1,1]

      ON EVERY ROW
         SELECT title INTO r_faorgn.title
         FROM orgn
         WHERE orgn.key_orgn = r_transact.fund
         AND orgn.yr = r_transact.yr

         IF l_debit IS NULL THEN
            LET l_debit = 0.00
         END IF
         IF l_credit IS NULL THEN
            LET l_credit = 0.00
         END IF
         LET l_debit = 0.00
         LET l_credit = 0.00

         LET l_debit = r_payroll.Debit
         LET l_credit = r_payroll.Credit

         LET total_debit = total_debit + l_debit
         LET total_credit = total_credit + l_credit

         IF p_rpt_dest = 'x' THEN
            INITIALIZE empl_no_xls,empl_name_xls TO NULL
            LET empl_no_xls = r_payroll.empl_no
            IF empl_no_xls = 0 THEN
            ELSE
               LET empl_no_xls = "("||empl_no_xls.trimLeftWhiteSpace()||")"
               LET empl_name_xls = r_payroll.f_name CLIPPED, ",",
                           r_payroll.l_name CLIPPED, " ", 
                           r_payroll.m_name CLIPPED, empl_no_xls CLIPPED
            END IF
            PRINT COLUMN 1, lvoid_man,                          #EFIN-69331
                  COLUMN 2, empl_name_xls,
                  COLUMN 3, r_payroll.pay_run,
                  COLUMN 4, lrun_desc,
                  COLUMN 5, r_payroll.pay_date,
                  COLUMN 6, r_payroll.Pay_Fund_Account,
                  COLUMN 7, ltitle,
                  COLUMN 8, r_payroll.rec_type,
                  COLUMN 9, l_debit,
                  COLUMN 10,l_credit
         ELSE 
            PRINT COLUMN 1,   r_payroll.pay_run,
                  COLUMN 11,  lrun_desc CLIPPED ,
                  COLUMN 26,  r_payroll.pay_date,
                  COLUMN 42,  r_payroll.Pay_Fund_Account,
                  COLUMN 75,  r_payroll.rec_type CLIPPED ,
                  COLUMN 108, l_debit,
                  COLUMN 118, l_credit
            PRINT COLUMN 42,  ltitle
         SKIP 1 LINE
         END IF

      PAGE HEADER
         LET empl_num = r_payroll.empl_no
         LET empl_num = "("||empl_num.trimLeftWhiteSpace()||")"
         LET empl_name_xls = r_payroll.f_name CLIPPED, ",", r_payroll.l_name CLIPPED, " ", r_payroll.m_name CLIPPED, empl_num clipped

      ON LAST ROW
         IF  p_rpt_dest = 't' OR p_rpt_dest = 'd'  THEN                             
            SKIP 1 LINE
            LET grd_total_debit = SUM(l_debit)
            LET grd_total_credit = SUM(l_credit)
            PRINT COLUMN 1,"Overall - Total ",
                  COLUMN 108,grd_total_debit,
                  COLUMN 118,grd_total_credit
            
         END IF

         IF  p_rpt_dest = 'x' AND mDataOption = 'A'  THEN
            SKIP 1 LINE
            LET grd_total_debit = SUM(l_debit)
            LET grd_total_credit = SUM(l_credit)
            PRINT COLUMN 1,"Overall - Total ",                  #EFIN-69331
                  COLUMN 9,grd_total_debit,
                  COLUMN 10,grd_total_credit
         END IF

END REPORT #payrollreg_rpt

#Genero 4RP Report Wr.
FUNCTION payroll_register_4rp(mDataOption)
   DEFINE mDataOption CHAR(1)
   DEFINE lhide_tol    INTEGER
   DEFINE lrun_desc   STRING
   DEFINE ltitle      STRING

   LET row_id = 0

   IF progress_total >= 1 THEN
      LET files = dirList(cCurrentDir)
      CALL files.sort("filename", FALSE)
      START REPORT rpt_payrollregister
         TO XML HANDLER reportConfiguration("payrollregister_rpt.4rp")

      FOREACH curs1
         INTO r_payroll.*
      SELECT run_desc INTO lrun_desc FROM paygroups WHERE paygroups.pay_run = r_payroll.pay_run AND group_x = '#'
      SELECT title INTO ltitle FROM faaccount WHERE faaccount.acct = r_payroll.acct
         CALL libProgressBarProcess()

         IF r_payroll.Debit <> 0 AND                         #EFIN-69331 -->
               r_payroll.Credit <> 0 THEN
               LET r_payroll.Debit = 0
               LET r_payroll.Credit = 0 
            END IF                                                    #EFIN-69331 <--

         IF int_flag THEN
            LET int_flag = FALSE
            LET user_inter = TRUE
            EXIT FOREACH
         END IF
         IF r_payroll.Debit = 0 AND r_payroll.Credit = 0 THEN   #EFIN-69331
            ELSE
            OUTPUT TO REPORT rpt_payrollregister(r_payroll.*,lrun_desc,ltitle,  row_id, lhide_tol, mDataOption)
         END IF                                                 #EFIN-69331
         LET row_id = row_id + 1
      END FOREACH

      CALL libProgressBarFinish(TRUE)

      FINISH REPORT rpt_payrollregister
      CALL saveReport()
   END IF

END FUNCTION #payroll_register_4rp

REPORT rpt_payrollregister(r_payroll,lrun_desc,ltitle,  row_id, lhide_tol, mDataOption)

   DEFINE r_payroll RECORD
          empl_no   LIKE detdist.empl_no,
          f_name    LIKE employee.f_name,
          m_name    LIKE employee.m_name,
          l_name    LIKE employee.l_name,
          rec_type  STRING,
          pay_run   LIKE detdist.pay_run,
          pay_date  LIKE detdist.pay_date,
          acct      LIKE detdist.acct,
          Pay_Fund_Account STRING ,
          Debit            DECIMAL(12,2),
          Credit           DECIMAL(12,2)
   END RECORD
   DEFINE lrun_desc    STRING
   DEFINE ltitle       STRING
   DEFINE mDataOption  CHAR(1)
   DEFINE row_id       INTEGER
   DEFINE lhide_tol    INTEGER
   DEFINE lvoid_man    STRING                                   #EFIN-69331

   FORMAT
      FIRST PAGE HEADER
         LET l_debit = 0.00
         LET l_credit = 0.00
         LET grd_total_debit = 0.00
         LET grd_total_credit = 0.00

         SELECT *
            INTO r_faorgn.*
            FROM orgn
            WHERE orgn.key_orgn = r_transact.fund
               AND orgn.yr = r_transact.yr

         LET currdate = DATE(CURRENT)
         LET currtime = TIME(CURRENT)
         LET pay_run_desc = "PAYROLL REGISTER FOR PAY RUN ",r_payroll.pay_run  CLIPPED, " ", " - ", " ", lrun_desc
         INITIALIZE empl_name TO NULL
         IF r_employee.empl_no IS NOT NULL THEN
               IF r_employee.empl_no = 0 THEN
               ELSE
               LET empl_no = r_payroll.empl_no
               LET empl_no = "("||empl_no.trimLeftWhiteSpace()||")"
                  LET empl_name = "EMPLOYEE : ", r_payroll.f_name CLIPPED, ",", r_payroll.l_name CLIPPED, " ", r_payroll.m_name CLIPPED, empl_no clipped
               END IF
         END IF

      ON EVERY ROW

         SELECT * INTO r_fam_prof.* FROM fam_prof
         SELECT title
            INTO r_faorgn.title
            FROM orgn
            WHERE orgn.key_orgn = r_transact.fund
               AND orgn.yr = r_transact.yr

         LET str_pay_date = ""
         IF r_payroll.pay_date IS NOT NULL THEN
            LET str_pay_date = r_payroll.pay_date
         END IF
         LET fullname =  r_payroll.f_name CLIPPED, " ", " - ", " ", r_payroll.l_name
         LET fund_title = r_transact.fund CLIPPED, " ", " - ", " ", r_faorgn.title

         LET lvoid_man = r_detdist.void_man                     #EFIN-69331 -->
         IF lvoid_man = 'P'  THEN
            LET lvoid_man = 'PAYRUN'
            LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF

         IF lvoid_man = 'V' THEN
          LET lvoid_man = 'VOID'
          LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF

         IF lvoid_man = 'M' THEN
            LET lvoid_man = 'MANUAL'
            LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF

         IF lvoid_man = 'R' THEN
            LET lvoid_man = 'REDISTRIBUTION'
            LET  lvoid_man = "BATCH TYPE : ",lvoid_man
         END IF                                                 #EFIN-69331 <--

         IF l_debit IS NULL THEN
            LET l_debit = 0.00
         END IF
         IF l_credit IS NULL THEN
            LET l_credit = 0.00
         END IF
         LET l_debit = 0.00
         LET l_credit = 0.00

         LET l_debit = r_payroll.Debit
         LET l_credit = r_payroll.Credit

         LET total_debit = total_debit + l_debit
         LET total_credit = total_credit + l_credit

         PRINTX r_payroll.empl_no,
                fullname,
                r_faaccount.title,
                ltitle,
                r_transact.fund,
                r_transact.account,
                r_payroll.pay_run,
                pay_run_desc,
                lrun_desc,
                r_payroll.rec_type,
                r_payroll.Pay_Fund_Account,
                r_payroll.acct,
                r_payroll.pay_date,
                str_pay_date,
                r_payroll.acct,
                r_fam_prof.pay_fund,
                lvoid_man,                                      #EFIN-69331
                row_id
         PRINTX currdate
         PRINTX currtime
         PRINTX r_fam_prof.company
         PRINTX empl_name
         PRINTX r_fam_prof.client
         PRINTX fund_title
         PRINTX l_debit
         PRINTX l_credit

      ON LAST ROW
         LET lhide_tol = row_id
         PRINT lhide_tol

         LET grd_total_debit = SUM(l_debit)
         LET grd_total_credit = SUM(l_credit)

         PRINTX mDataOption
         PRINTX grd_total_debit
         PRINTX grd_total_credit

END REPORT #rpt_payrollregister

FUNCTION reportConfiguration(rpName STRING) RETURNS om.SaxDocumentHandler

   DEFINE lProcStatus SMALLINT

# load the 4rp file
   IF NOT fgl_report_loadCurrentSettings(rpName) THEN
      RETURN NULL
   END IF

   IF p_rpt_dest = "t" THEN
      LET reportFileName = "payrollregister.pdf"
      CALL fgl_report_selectDevice("PDF")
      CALL fgl_report_selectPreview(FALSE)

      CALL fgl_report_setOutputFileName(reportFileName)
      LET tmpFilename = reportFileName
      LET hnd1 = fgl_report_commitCurrentSettings()
   END IF

   IF p_rpt_dest = "d" THEN
      LET int_flag = FALSE
      CALL fgl_report_setOutputFileName(p_rpt_name)
      CALL fgl_report_selectPreview(FALSE)
      CALL fgl_report_selectDevice("PDF")
   END IF

   RETURN fgl_report_commitCurrentSettings()

END FUNCTION #reportConfiguration

FUNCTION dirList(dirPath STRING) RETURNS(DYNAMIC ARRAY OF t_fileinfo)

   DEFINE myList DYNAMIC ARRAY OF t_fileinfo
   DEFINE idx      INTEGER
   DEFINE handle   INTEGER
   DEFINE filename STRING

   LET idx = 0
   IF dirPath == cReportDir THEN
      LET dirPath = SFMT(cReportDir, os.Path.separator())
   END IF
   LET handle = os.Path.dirOpen(dirPath)
   WHILE handle > 0
      LET filename = os.Path.dirNext(handle)
      IF filename IS NULL THEN
         EXIT WHILE
      END IF
      IF filename == "." OR filename == ".." THEN
         CONTINUE WHILE
      END IF
      LET idx = idx + 1
      LET myList[idx].filename = filename
      LET myList[idx].filesize =
         os.Path.size(SFMT("%1%2%3", dirPath, os.Path.separator(), filename))
      LET myList[idx].modtime =
         os.Path.mtime(SFMT("%1%2%3", dirPath, os.Path.separator(), filename))
   END WHILE

   CALL os.Path.dirClose(handle)
   RETURN myList

END FUNCTION #dirList

FUNCTION saveReport() RETURNS()
   DEFINE result       BOOLEAN
   DEFINE saveFilename STRING

   LET saveFilename = SFMT("..%1reports%1%2",
         os.Path.separator(), os.Path.baseName(tmpFilename))

   LET result = os.Path.copy(tmpFilename, saveFilename)
   IF result = 0 THEN
      DISPLAY SFMT("File %1 saved!", saveFilename)
      IF p_rpt_dest = "t" THEN
         CALL FGL_PUTFILE(tmpFilename, os.Path.baseName(saveFilename))
      END IF
      IF p_rpt_dest = "x" THEN
         CALL FGL_PUTFILE(tmpFilename, os.Path.baseName(saveFilename))
      END IF
   ELSE
      DISPLAY SFMT("File %1 NOT saved!", saveFilename)
   END IF

END FUNCTION #saveReport
